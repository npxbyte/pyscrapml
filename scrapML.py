#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import urllib3
from lxml import html, etree
from store_logs import StoreProds

class Scrap: 

    def __init__(self):
        self.url = 'http://listado.mercadolibre.com.ar/'
        self.product_list = []
        
    def __page(self):
        '''
        Obtiene los resultados de la url pasada como parametro
        '''
        http = urllib3.PoolManager()
        r = http.request('GET', self.url)  # r.status r.data
        doc = html.document_fromstring(r.data)
        productos = doc.cssselect('li.list-view-item')   
    
        for producto in productos:
            #print(html.tostring(producto))
     
            titulo    = producto.cssselect('a img')[0].attrib['alt']
            link      = producto.cssselect('a')[0].attrib['href']
            tipo      = producto.cssselect('li.extra-info-condition')[0].text.strip()
            
            try:    
                ubicacion = producto.cssselect('li.extra-info-location')[0].text.strip()
            except:
                ubicacion = ""

            try:
                centavos = producto.cssselect('strong.ch-price sup')[0].text.strip()
                precio    = str(producto.cssselect('strong.ch-price')[0].text.replace('.','') + '.' + centavos).strip()
            except:
                precio = "0"
    
            try:
                imagen    = producto.cssselect('a img.loading')[0].attrib['title']
            except:
                imagen    = producto.cssselect('a img')[0].attrib['src']
        
            prod = dict(titulo=titulo, link=link, tipo=tipo, ubicacion=ubicacion, precio=precio, imagen=imagen)
            self.product_list.append(prod)
         
        try:
            next_page = doc.cssselect('ul.ch-pagination li a.prefetch')[0].attrib['href']
        except:
            next_page = None

        return dict(current_page=self.url, next_page=next_page)

    
    def get_products(self, filtro=None):
        
        if filtro:
            self.url += '/' + filtro

        '''
        Obtiene los resultados de todas las paginas, inclusive sigue los links del paginador
        '''
        while(self.url!=None):
            result = self.__page()
            self.url = result["next_page"]
        return self.product_list
        

if __name__ == '__main__':


    parser = argparse.ArgumentParser()
    # Argumento posicional
    parser.add_argument("nombre", type=str, help="Nombre del producto a buscar")
    # Agumento optativo
    parser.add_argument("-c", "--comienzan_hoy", action="count", default=0, help="Muestra solo los productos publicados hoy")
    parser.add_argument("-db", "--guardar_db", action="count", default=0, help="Guardar resultado en una DB sqlite")
    parser.add_argument("-pmin", "--precio_minimo", type=int, default=0)
    parser.add_argument("-pmax", "--precio_maximo", type=int, default=0)
    # Grupo (se puede usar un solo termino) para mostrar segun estado 
    group_estado = parser.add_mutually_exclusive_group()
    group_estado.add_argument("-n", "--nuevos", action="count", help="Productos nuevos")
    group_estado.add_argument("-u", "--usados", action="count", help="Productos usados")
    # Grupo para ordenar por precio
    group_orden = parser.add_mutually_exclusive_group()
    group_orden.add_argument("-a", "--ascendente", action="count", help="Ordena productos por precio ascendente")
    group_orden.add_argument("-d", "--descendente", action="count", help="Ordena productos por precio descendente")

    args = parser.parse_args()


    # Etapa filtros
    filtro = ''

    if args.ascendente:
        filtro += '_OrderId_PRICE'
    elif args.descendente:
        filtro += '_OrderId_PRICE*DESC'

    if args.comienzan_hoy > 0:
        filtro += '_OtherFilterID_COMHOY'                

    if args.nuevos:
        filtro += '_ItemTypeID_N'
    elif args.usados:
        filtro += '_ItemTypeID_U' 

    if args.precio_minimo > 0 or args.precio_maximo > 0:
        filtro +='_PriceRange_' + str(args.precio_minimo) + '-' + str(args.precio_maximo)

    filtro = args.nombre + filtro 
    # Fin etapa filtros

    products = Scrap().get_products(filtro)

    # Verifico accion: guardar en DB o imprimir en pantalla.
    if args.guardar_db:
        db = StoreProds()
        db.insert_data(products)
    else:
        for product in products:
            print(product['precio'] + ' ' + product['titulo'] + ' (' + product['tipo'] + ')\n' + product['link'] + '\n')

