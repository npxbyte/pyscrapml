from sqlalchemy                 import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm             import relation, sessionmaker

engine = create_engine('sqlite:///storage.db', echo=False)
Session     = sessionmaker(bind=engine)
Base = declarative_base()
metadata = Base.metadata

class ProdRecv(Base):
    __tablename__ = 'product'
    id	= Column(Integer, primary_key=True)
    titulo = Column(String(150),nullable=False)
    link = Column(String(150),nullable=False)
    tipo = Column(String(150),nullable=False)
    ubicacion = Column(String(150),nullable=False)
    precio = Column(String,nullable=False)
    imagen = Column(String(150),nullable=False)

    def __init__(self, titulo, link, tipo, ubicacion, precio, imagen):
        self.titulo = titulo if titulo.strip()!="" else None
        self.link = link if link.strip()!="" else None
        self.tipo = tipo if tipo.strip()!="" else None
        self.ubicacion = ubicacion if ubicacion.strip()!="" else None
        self.precio = precio if precio.strip()!="" else None
        self.imagen = imagen if imagen.strip()!="" else None


metadata.drop_all(engine) # Se elimina el esquema por completo
metadata.create_all(engine)


class StoreProds(object):
    def __init__(self):
        self.sesion = Session()

    def first_data(self):
        query = self.sesion.query(ProdRecv).first()
        return query

    def insert_data(self, titulo, link, tipo, ubicacion, precio, imagen):
        collected_data = ProdRecv(titulo, link, tipo, ubicacion, precio, imagen)
        self.sesion.add(collected_data)
        self.sesion.commit()

    def insert_data(self, lista_productos):
        for producto in lista_productos:
            collected_data = ProdRecv(producto["titulo"], producto["link"], producto["tipo"], producto["ubicacion"], producto["precio"], producto["imagen"])
            self.sesion.add(collected_data)
        self.sesion.commit()

    def delete_data(self, id):
        query = self.sesion.query(ProdRecv).filter_by(id=id).delete()
        self.sesion.commit()
        return query

    def all(self):
        query = self.sesion.query(ProdRecv)
        return query
